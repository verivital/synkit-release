# SynKit
## A tool for synthesis of LTL specifications via automated planning.

Algorithms
==========
This software implements:

* the algorithms for LTL synthesis presented at IJCAI-18.
    * LTL Realizability via Safety and Reachability Games. A. Camacho, C. J. Muise, J. A. Baier, and S. A. McIlraith, S. A. In Proceedings of the Twenty-Seventh International Joint Conference on Artificial Intelligence (IJCAI), pages 4683-4691, 2018.
* the algorithms for LTL synthesis (for a subset of LTL) presented at  Canadian AI'18 (and Genplan-17).
    * Synthesizing Controllers: On the Correspondence Between LTL Synthesis and Non-deterministic Planning. A. Camacho, J. A. Baier, C. J. Muise, and S. A. McIlraith. In Advances in Artificial Intelligence - Proceedings of the Thirty-First Canadian Conference on Artificial Intelligence (AI 18), pages 45–59, 2018.
* the algorithms for Finite LTL synthesis presented at ICAPS-18.
    * Finite LTL Synthesis as Planning. A. Camacho, J. A. Baier, C. J. Muise, and S. A. McIlraith. In Proceedings of the Twenty-Eight International Conference on Automated Planning and Scheduling (ICAPS), pages 29–38, 2018.

Online Tool
===========
The algorithms are accessible in the webservice SynKit, accessible through the following url:

http://www.cs.toronto.edu/~acamacho/synkit

If your research makes use of the SynKit webservice, please cite the following paper and the paper that corresponds to the algorithms used for the compilation to planning (IJCAI-18, Canadian AI'18, or ICAPS-18).

* SynKit: LTL Synthesis as a Service. A. Camacho, A. C. J. Muise, J. A. Baier, and S. A. McIlraith. In Demonstration Track at the Twenty-Seventh International Joint Conference on Artificial Intelligence (IJCAI), pages 5817–5819, 2018.

SynKit was awarded with the Best System Demonstration Award at ICAPS-18.


References
==========

The citations are listed below.

    @inproceedings{cam-mui-bai-mci-ijcai18,
      author    = {Alberto Camacho and
                   Christian J. Muise and
                   Jorge A. Baier and
                   Sheila A. McIlraith},
      title     = {{LTL} Realizability via Safety and Reachability Games},
      booktitle = {Proceedings of the Twenty-Seventh International Joint Conference on Artificial Intelligence ({IJCAI})},
      pages     = {4683--4691},
      year      = {2018}
    }



    @inproceedings{cam-bai-mui-mci-ccai18,
      author    = {Alberto Camacho and
                   Jorge A. Baier and
                   Christian J. Muise and
                   Sheila A. McIlraith},
      title     = {Synthesizing Controllers: On the Correspondence Between {LTL} Synthesis and Non-deterministic Planning},
      booktitle = {Advances in Artificial Intelligence - Proceedings of the Thirty-First Canadian Conference on Artificial Intelligence ({CCAI})},
      pages     = {45--59},
      year      = {2018}
    }
    
    
    @inproceedings{cam-bai-mui-mci-icaps18,
      author    = {Alberto Camacho and
                   Jorge A. Baier and
                   Christian J. Muise and
                   Sheila A. McIlraith},
      title     = {Finite {LTL} Synthesis as Planning},
      booktitle = {Proceedings of the Twenty-Eight International Conference on Automated Planning and Scheduling ({ICAPS})},
      pages     = {29--38},
      year      = {2018}
    }
    
    @inproceedings{cam-mui-bai-mci-ijcai18demo,
      author    = {Alberto Camacho and
                   Christian J. Muise and
                   Jorge A. Baier and
                   Sheila A. McIlraith},
      title     = {{SynKit}: {LTL} Synthesis as a Service},
      booktitle = {Demonstration Track at the Twenty-Seventh International Joint Conference on Artificial Intelligence ({IJCAI})},
      year      = {2018},
      pages     = {5817--5819}
    }


    
Installation
============

The translator code is written in python3. 
Getting in to work has a number of fependencies:

- Download submodules:
    ```bach
        git submodule update --init
    ```

- **TLSF parser:** Uses a modified version of Syfco to parse input TLSF files.
    * Install Haskell: `url -sSL https://get.haskellstack.org/ | sh`
    ``` bash
        cd src/parser
        cd syfco
        stack install
    ```

- **LTL to Automata:** Currently implements support for Spot and LTL3BA
    * Spot:
    ``` bash
        add-apt-repository -y ppa:ubuntu-toolchain-r/test
        echo 'deb http://www.lrde.epita.fr/repo/debian/ stable/' >> /etc/apt/sources.list
        apt-get update
        apt-get install spot libspot-dev spot-doc python3-spot
    ```
    
    * LTL3BA:
    
        * Install Buddy from: `wget https://sourceforge.net/projects/buddy/files/latest/download`
    
        * Install LTL3BA from `wget https://cytranet.dl.sourceforge.net/project/ltl3ba/ltl3ba/1.1/ltl3ba-1.1.3.tar.gz`.
            Then, modify this line in `src/translator/automata.py`
        ``` python
            LTL3BA = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'ltl3ba-1.1.3/ltl3ba') 
        ```
    * LTLfKit: Only needed for the use of the compilations for finite LTL synthesis
    
- **FOND planner:** Optional. Currently implements support for PRP, myND, and strong-FIP.

- **KRToolKit:** `hg clone ssh://hg@bitbucket.org/haz/krtoolkit`

Basic Usage
===========

LTL Synthesis
-------------

SynKit can perform four types of searches. 
For an LTL synthesis problem $<X,Y,\varphi>_{Mealy}$, the following commands perform, respectively:

* Synthesis

* Realizability

* Unrealizability certificate generation

* Unrealizability

``` bash
    python3 src/synkit-inf.py --config MealyLTLSynthesis --tlsf $benchset/$filename.tlsf --output-prefix temp/$benchset/K$k/$filename
    python3 src/synkit-inf.py --config MealyLTLRealizability --tlsf $benchset/$filename.tlsf --output-prefix temp/$benchset/K$k/$filename
    python3 src/synkit-inf.py --config MealyLTLCertificate --tlsf $benchset/$filename.tlsf --output-prefix temp/$benchset/K$k/$filename
    python3 src/synkit-inf.py --config MealyLTLUnrealizability --tlsf $benchset/$filename.tlsf --output-prefix temp/$benchset/K$k/$filename
```

The default parameters can be overriden. 
In particular, parameter $k$ can be set with the flag ``--k-coBuchi <k>``.
"Mealy" and "Moore" stands for which player plays first. 
If the agent plays first, select "Moore". 
If the environment plays first, select "Mealy".
See ``defaults.cfg`` file for more details.


Finite LTL Synthesis
--------------------

SynKit can search for a winning strategy, of a certificate of unrealizability.
For a Finite LTL synthesis problem $<X,Y,\varphi>_{Mealy}$, the following commands perform, respectively:

* Synthesis

* Unrealizability certificate generation

``` bash
    python3 src/synkit-fin.py --config MealyLTLfSynthesis --tlsf $benchset/$filename.tlsf --output-prefix temp/$benchset/K$k/$filename
    python3 src/synkit-fin.py --config MealyLTLfCertificate --tlsf $benchset/$filename.tlsf --output-prefix temp/$benchset/K$k/$filename
```

Acknowledements
---------------

We thank NSERC for the funding provided to Alberto Camacho to conduct research and implement this software.