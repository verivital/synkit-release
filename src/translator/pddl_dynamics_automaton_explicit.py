class Options:
    def __init__(self,opts):
        self.opts = opts
        assert(opts.k_coBuchi!=None)
        assert(opts.output_domain)
        assert(opts.output_instance)
        assert(opts.fixed_var_ordering!=None)
        if opts.width_bound:
            assert(self.fixed_var_ordering())
        
    def k_coBuchi(self):
        return self.opts.k_coBuchi
    
    def output_domain(self):
        # return "%s_domain.pddl" % self.opts.output
        return self.opts.output_domain

    def output_instance(self):
        # return "%s_instance.pddl" % self.opts.output
        return self.opts.output_instance
    
    def fixed_var_ordering(self):
        return self.opts.fixed_var_ordering
        
    def width_bound(self):
        return self.opts.width_bound
        
    def decompose_automata(self):
        return self.opts.split_spec
    
    def game_semantics(self):
        return self.opts.game_semantics

class Action:
    name = None
    param = None
    pre = None
    effs = None
    def to_pddl(self):
        assert(len(self.effs) > 0)
        pddl = "\n\t(:action %s" % self.name
        if self.param:
            pddl += "\n\t\t:parameters %s" % self.param
        precs_pddl = "(and %s)" % (" ".join(self.pre))
        if len(self.effs) > 1:
            effs_pddl = "(oneof \n\t\t\t" + "\n\t\t\t".join( "(and %s)" %  " ".join(eff_k) for eff_k in self.effs  ) + "\n\t\t\t)\n\t\t"
        elif len(self.effs) == 1:
            effs_pddl = "(and %s)" % " ".join(self.effs[0])
        pddl += "\n\t\t:precondition %s" % precs_pddl
        pddl += "\n\t\t:effect %s" % effs_pddl
        pddl +="\n\t)"

        return pddl

class AutomatonDynamics():
    def __init__(self, aut, aut_id):
        self.aut = aut
        self.aut_id = aut_id
        
    def q(self,k):
        return "(aut_%s_q_%s)" % (self.aut_id, k)
    def qt(self,k):
        return "(aut_%s_qt_%s)" % (self.aut_id, k)
    def qs(self,k):
        return "(aut_%s_qs_%s)" % (self.aut_id, k)
    def qst(self,k):
        return "(aut_%s_qst_%s)" % (self.aut_id, k)
    def can_accept(self):
        return "(aut_%s_can_accept)" % self.aut_id

    def aut_predicates(self):
        predicates = ""
        # automaton fluents, sync, tokens, sync-tokens
        predicates += " \n\t" + " ".join( self.q(k) for k in range(0, self.aut.num_states()))
        predicates += " \n\t" + " ".join( self.qt(k) for k in range(0, self.aut.num_states()))
        predicates += " \n\t" + " ".join( self.qs(k) for k in range(0, self.aut.num_states()))
        predicates += " \n\t" + " ".join( self.qst(k) for k in range(0, self.aut.num_states()))
        # fluent to control accepting states
        predicates += "\n\t" + self.can_accept()
        return predicates


class PDDLDynamics():
    def __init__(self, uncontrollable_vars, controllable_vars, opts):
        self.controllable_vars = controllable_vars
        self.uncontrollable_vars = uncontrollable_vars
        self.automata = []
        self.opts = Options(opts)

    def write_pddl_domain(self):
        print("Writing PDDL Domain")
        domain_file = open(self.opts.output_domain(),"w")

        self.write_header(domain_file)
        self.write_environment_actions(domain_file)
        self.write_switch_env2aut_actions(domain_file)
        self.write_automata_actions(domain_file)
        self.write_switch_aut2env_actions(domain_file)
        # self.write_advance_horizon_action(domain_file)
        self.write_accept_actions(domain_file)
        domain_file.write(")")
        domain_file.close()
    
    def write_pddl_instance(self):
        max_horizon = self.opts.k_coBuchi()
        print("Writing PDDL Instance")
        instance_file = open(self.opts.output_instance(),"w")

        # fluents controlling the search horizon
        objects = "\n\t" + " ".join("h%s" % h for h in range(0, max_horizon) ) + " - counter"
        # horizon 0
        init = "(at-horizon h0) (first-horizon h0)"
        # 
        init += "\n\t" + " ".join("(next h%s h%s)" % (h+1,h) for h in range(0, max_horizon -1) )
        init += "\n\t" + "(environment_mode)"
        init += "\n\t" + "(varturn_%s)" % self.uncontrollable_vars[0]
        for aut_dynamics in self.automata:
            q_init = aut_dynamics.aut.get_init_state_number()
            init += "\n\t" + aut_dynamics.q(q_init)
        if self.opts.width_bound():
            # n_trans = sum(len(automaton.aut.get_transitions()) for automaton in self.automata) 
            # n_trans = width
            objects += "\n\t" + " ".join("t%s" % t for t in range(self.opts.width_bound() + 1)) + " - trans_counter"
            init += "\n\t" + "(at_trans_turn t0) (first_trans_turn t0)"
            init += "\n\t" + " ".join("(trans_next t%s t%s)" % (t+1,t) for t in range(self.opts.width_bound()) )
            
        header = """(define (problem prob)\n\t(:domain dom)\n\t(:objects %s)\n\t(:init %s)\n\t(:goal (dummy_goal))\n)""" % (objects, init)
        instance_file.write(header)
        instance_file.close()
        
    def add_automaton(self,aut):
        self.automata.append(AutomatonDynamics(aut, len(self.automata)))
        
    def write_header(self, pddl_file):
        # fluents controlling the search horizon
        self.types = "\n\t" + " counter"

        self.predicates = " \n\t" + "(at-horizon ?h - counter) (next ?h1 - counter ?h2 - counter) (first-horizon ?h - counter) "
        # variable fluents: "literals"
        self.predicates += " \n\t" + " ".join("(pos_%s)" % x for x in self.uncontrollable_vars)
        self.predicates += " \n\t" + " ".join("(neg_%s)" % x for x in self.uncontrollable_vars)
        self.predicates += " \n\t" + " ".join("(pos_%s)" % y for y in self.controllable_vars)
        self.predicates += " \n\t" + " ".join("(neg_%s)" % y for y in self.controllable_vars)
        # fluents to control modes
        self.predicates += "\n\t" + "(automaton_mode) (environment_mode)"
        # fluents to control turns for uncontrollable variable assignments
        self.predicates += " \n\t" + " ".join("(varturn_%s)" % x for x in self.uncontrollable_vars)
        # dummy goal fluent
        self.predicates += "\n\t" + "(dummy_goal)"
        # 
        if self.opts.width_bound():
             self.predicates += " \n\t" + "(at_trans_turn ?t - trans_counter) (trans_next ?t1 - trans_counter ?t2 - trans_counter) (first_trans_turn ?t - trans_counter) "
             self.types += "\n\t" + " trans_counter"
        # Write header
        compilation_predicates = self.predicates
        for aut_dynamics in self.automata:
            compilation_predicates +=  aut_dynamics.aut_predicates() 

        header = """(define (domain dom)
        (:requirements :typing :strips :negative-preconditions :non-deterministic)
        (:types %s)
        (:predicates %s)
        """ % (self.types,compilation_predicates)
        pddl_file.write(header)
    
    def write_automata_actions(self, pddl_file):
        for aut_dynamics in self.automata:
            self.write_automaton_actions(aut_dynamics,pddl_file)

    def write_automaton_actions(self, aut_dynamics, pddl_file):
        # Write actions to set Y variables via automaton transitions
        Q_Fin = aut_dynamics.aut.get_accepting()
        transitions = aut_dynamics.aut.get_transitions()
        # print(Q_Fin)
        for t in transitions:
            # print("%s-->%s \t %s" % (t.src_id, t.dst_id,t.guard))
            action = self.write_transition_action(aut_dynamics, t, t.dst_id in Q_Fin)
            pddl_file.write(action.to_pddl())
            
    def write_transition_action(self, aut_dynamics, transition, goesto_accepting):
        action = Action()
        action.name = "aut_%s_transition_%s_%s_%s" % (aut_dynamics.aut_id, transition.src_id, transition.dst_id, transition._id)
        action.pre = ['(automaton_mode)', aut_dynamics.qs(transition.src_id), '(not %s)' % aut_dynamics.q(transition.dst_id)]
        action.effs = []
        eff = []
        if self.opts.width_bound():
            action.param = "(?t1 ?t2 - trans_counter)"
            action.pre += ["(at_trans_turn ?t1)", "(trans_next ?t2 ?t1)"]
            eff.extend(['(at_trans_turn ?t2)', '(not (at_trans_turn ?t1))'])
        for literal in transition.guard:
            if '!' in literal:
                action.pre.append( "(not (pos_%s) )" % literal.replace("!","") )
                eff.append( "(neg_%s)" % literal.replace("!","") )
            else:
                action.pre.append( "(not (neg_%s) )" % literal )
                eff.append( "(pos_%s)" % literal)
                
        eff.append( aut_dynamics.q(transition.dst_id) )
        # eff.append( "(when %s %s )" % (self.qs(transition.src_id), self.q(transition.dst_id)  )  )
        if goesto_accepting:
            eff.append( aut_dynamics.can_accept() )
            # eff.append( "(when %s %s )" % (self.qst(transition.src_id), self.q(transition.dst_id)  )  )
        if not goesto_accepting:
            eff.append( "(when %s %s )" % (aut_dynamics.qst(transition.src_id), aut_dynamics.qt(transition.dst_id)  )  )
        action.effs.append(eff)
        
        return action

    def write_advance_horizon_action(self, pddl_file):
        action = """\n\t(:action advance_horizon
		:parameters (?h1 ?h2 - counter)
		:precondition (and (automaton_mode) (at-horizon ?h1) (next ?h2 ?h1))
		:effect (and (not (at-horizon ?h1)) (at-horizon ?h2))
	    )\n"""
        pddl_file.write(action)
        
    def write_environment_actions(self, pddl_file):
        # Write cascade of actions to set X variables
        for i in range(len(self.uncontrollable_vars) -1):
            action = Action()
            action.name = "env_move_%s" % self.uncontrollable_vars[i]
            action.pre = ["(environment_mode)", "(varturn_%s)" % self.uncontrollable_vars[i] ]
            eff1 = ["(pos_%s)" % self.uncontrollable_vars[i], "(not (varturn_%s))" % self.uncontrollable_vars[i], "(varturn_%s)" % self.uncontrollable_vars[i+1] ]
            eff2 = ["(neg_%s)" % self.uncontrollable_vars[i], "(not (varturn_%s))" % self.uncontrollable_vars[i], "(varturn_%s)" % self.uncontrollable_vars[i+1] ]
            action.effs = [eff1,eff2]
            pddl_file.write(action.to_pddl())
        
        action = Action()
        action.name = "env_move_%s" % self.uncontrollable_vars[len(self.uncontrollable_vars) -1]
        action.pre = ["(environment_mode)", "(varturn_%s)" % self.uncontrollable_vars[len(self.uncontrollable_vars) -1] ]
        eff1 = ["(pos_%s)" % self.uncontrollable_vars[len(self.uncontrollable_vars) -1], "(not (varturn_%s))" % self.uncontrollable_vars[len(self.uncontrollable_vars) -1] ]
        eff2 = ["(neg_%s)" % self.uncontrollable_vars[len(self.uncontrollable_vars) -1], "(not (varturn_%s))" % self.uncontrollable_vars[len(self.uncontrollable_vars) -1] ]
        action.effs = [eff1,eff2]
        pddl_file.write(action.to_pddl())

    def write_switch_env2aut_actions(self, pddl_file):
        # Write action to switch mode from environment_mode to automaton_mode
        action = Action()
        action.name = "switch_environment_to_automaton_mode"
        action.param = "(?h1 ?h2 - counter)"
        action.pre = ["(environment_mode)"]
        action.pre += ["(at-horizon ?h1)", "(next ?h2 ?h1)"]
        action.pre += ["(not (varturn_%s))" % x for x in self.uncontrollable_vars]
        eff = ["(not (environment_mode))", "(automaton_mode)"] 
        for aut_dynamics in self.automata:
            eff += ["(when %s (and %s (not %s) )  )" % (aut_dynamics.q(state), aut_dynamics.qs(state), aut_dynamics.q(state) ) for state in range(0, aut_dynamics.aut.num_states()) ]
            eff += ["(when %s (and %s (not %s) )  )" % (aut_dynamics.qt(state), aut_dynamics.qst(state), aut_dynamics.qt(state) ) for state in range(0, aut_dynamics.aut.num_states()) ]
        eff += ["(not (at-horizon ?h1))", "(at-horizon ?h2)"]
        action.effs = [eff]
        pddl_file.write(action.to_pddl())       
        
    def write_switch_aut2env_actions(self, pddl_file):
        # Write action to switch mode, deliveratively, from  automaton_mode to environment_mode
        action = Action()
        action.name = "switch_automaton_to_environment_mode"
        action.pre = ["(automaton_mode)"]
        eff = ["(not (automaton_mode))", "(environment_mode)"] 
        eff += ["(varturn_%s)" % self.uncontrollable_vars[0] ]
        # Regularization
        if self.opts.width_bound():
            action.param = "(?t ?t0 - trans_counter)"
            action.pre += ["(at_trans_turn ?t)", "(first_trans_turn ?t0)"]
            eff += ['(at_trans_turn ?t0)', '(not (at_trans_turn ?t))']
        for aut_dynamics in self.automata:
            eff += ["(not %s)" % aut_dynamics.qs(state) for state in range(0, aut_dynamics.aut.num_states()) ]
            eff += ["(not %s)" % aut_dynamics.qst(state) for state in range(0, aut_dynamics.aut.num_states()) ]
        eff += ["(not (pos_%s))" % x for x in self.uncontrollable_vars ]
        eff += ["(not (neg_%s))" % x for x in self.uncontrollable_vars ]
        eff += ["(not (pos_%s))" % y for y in self.controllable_vars ]
        eff += ["(not (neg_%s))" % y for y in self.controllable_vars ]
        # eff += ["(not (at-horizon ?h1))", "(at-horizon ?h2)"]
        # eff += ["(when (at-horizon ?h%s) (at-horizon ?h%s) )" % (h,h+1) for h in range(max_horizon-1)]
        action.effs = [eff]
        pddl_file.write(action.to_pddl())      


    def write_accept_actions(self, pddl_file):
        # Write action to recognize accepting states
        # todo: make sure that we copy q -> qT for non-accepting states.
        action = Action()
        action.name = "accept"
        action.param = "(?h ?h0 - counter)"
        action.pre = ["(automaton_mode)"]
        action.pre += [aut_dynamics.can_accept() for aut_dynamics in self.automata]
        action.pre += ["(at-horizon ?h)", "(first-horizon ?h0)"]
        eff1 = ["(dummy_goal)"]
        eff2 = ["(not %s)" % aut_dynamics.can_accept() for aut_dynamics in self.automata]
        eff2 += ["(varturn_%s)" % self.uncontrollable_vars[0] ]
        eff2 += ["(not (automaton_mode))", "(environment_mode)"]
        
        eff2 += ["(not (pos_%s))" % x for x in self.uncontrollable_vars ]
        eff2 += ["(not (neg_%s))" % x for x in self.uncontrollable_vars ]
        eff2 += ["(not (pos_%s))" % y for y in self.controllable_vars ]
        eff2 += ["(not (neg_%s))" % y for y in self.controllable_vars ]
        if self.opts.width_bound():
            action.param = "(?h - counter ?h0 - counter ?t - trans_counter ?t0 - trans_counter)"
            action.pre += ["(at_trans_turn ?t)", "(first_trans_turn ?t0)"]
            eff2 += ['(at_trans_turn ?t0)', '(not (at_trans_turn ?t))']
        for aut_dynamics in self.automata:
            # Regularization
            eff2 += ["(not %s)" % aut_dynamics.qs(state) for state in range(0, aut_dynamics.aut.num_states()) ]
            eff2 += ["(not %s)" % aut_dynamics.qst(state) for state in range(0, aut_dynamics.aut.num_states()) ]
            
            # eff2 += ["(when %s (and %s (not %s)) )" % (aut_dynamics.q(state), aut_dynamics.qt(state), aut_dynamics.q(state) ) for state in aut_dynamics.aut.get_accepting() ]
            eff2 += ["(when (and %s (not %s)) %s )" % (aut_dynamics.q(state), aut_dynamics.qt(state), aut_dynamics.qt(state) ) for state in range(0, aut_dynamics.aut.num_states()) ]
            not_accepting = [k for k in range(0, aut_dynamics.aut.num_states()) if k not in aut_dynamics.aut.get_accepting()]
            eff2 += ["(when (and %s %s) (and (not %s) (not %s)) )" % (aut_dynamics.q(state), aut_dynamics.qt(state), aut_dynamics.q(state), aut_dynamics.qt(state) ) for state in not_accepting ]

        eff2 += ["(not (at-horizon ?h))", "(at-horizon ?h0)"]
        action.effs = [eff1,eff2]
        pddl_file.write(action.to_pddl())
        
    # for statistical analysis
    def statistics(self):
        return 0,0