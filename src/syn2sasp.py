# This file is part of SynKit, a tool for LTL synthesis via automated planning.
# Copyright (C) 2018  Alberto Camacho <acamacho@cs.toronto.edu>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python3

from parser.parser import Parser
from translator.automata import Automaton
from os import system

SAVE_STATITSTICS = True

def main(opts, automata_alanet = None):
    print("Parsing TLSF file %s" % opts.tlsf)
    parser = Parser(opts.tlsf)
    X = parser.uncontrollable_vars()
    Y = parser.controllable_vars()

    # importing the desired compilation
    if opts.game_type == "nfw":
        if opts.split_spec in ['tlsf_components']:
            from translator.nfw_conj_game_2_planning import SASpDynamics
        else:
            from translator.nfw_game_2_planning import SASpDynamics
    elif opts.game_type == "u0cw":
        if opts.split_spec in ['tlsf_components']:
            from translator.cufw_disj_game_2_planning import SASpDynamics
        else:
            from translator.cufw_game_2_planning import SASpDynamics
    elif opts.game_type == "nkbw":
        from translator.nkbw_game_2_planning import SASpDynamics
    elif opts.game_type == "ukcw":
        from translator.ukcw_game_2_planning import SASpDynamics
    else:
        raise("Automaton compilation not supported.")
    
    if not automata_alanet:
        automata_alanet = parser.get_alanet(opts)
    automata = automata_alanet.automata
    
    if opts.semantics == opts.game_semantics:
        saspDynamics = SASpDynamics(X,Y,opts)
    else:
        assert( opts.semantics != opts.game_semantics)
        saspDynamics = SASpDynamics(Y,X,opts)

    for aut in automata:
        saspDynamics.add_automaton(aut)
        # print(aut.aut.to_str('hoa'))
    
    if opts.aut_type == "nfw":
        if opts.split_spec == "none":
            saspDynamics.add_alanet(automata_alanet)
            
    saspDynamics.write_sasp_file()

    if SAVE_STATITSTICS:
        n_fluents,n_actions = saspDynamics.statistics()
        return n_fluents,n_actions

if __name__ == "__main__":
    import options
    
    opts = options.parse_options()
    main(opts)