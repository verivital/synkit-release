# This file is part of SynKit, a tool for LTL synthesis via automated planning.
# Copyright (C) 2018  Alberto Camacho <acamacho@cs.toronto.edu>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/python

import syn2pddl,syn2sasp
from parser.parser import Parser
from translator.automata import Automaton
from planner.planner import Planner

SAVE_STATITSTICS = False

def get_aut_fml_pairs(node):
    aut_fml_pairs = []
    if node.siblings != None:
        for sibling in node.siblings:
            aut_fml_pairs.extend(get_aut_fml_pairs(sibling))
        return aut_fml_pairs
    else:
        return [(node.aut_id, node.fml)]
    
def statistics(automata_alanet,planner,solution_found):
    import os
    problem_filename = os.path.basename(opts.tlsf)
    
    automata_runtime = automata_alanet.automata_time
    automaton_sizes = [automaton.num_states() for automaton in automata_alanet.automata]

    subformulae = []
    for key in automata_alanet.subformulae_dict:
        subformulae.extend(automata_alanet.subformulae_dict[key] )
    subformulae_sizes = [ sum(subformula.count(op) for op in ['X','G', 'U', 'F', 'W', 'N', 'R','||', '&&']) for subformula in subformulae ]
    planner_runtime = planner.runtime()
    planner_policy_time = planner.policy_time()
    planner_policy_size = planner.policy_size()
    # nodes_visited = planner.nodes_visited()
    n_variables = planner.n_variables()
    n_autstate_variables = planner.n_autstate_variables()
    
    # print(automaton_sizes)
    # if solution_found:
    #     return problem_filename,automata_runtime,automaton_sizes,subformulae_sizes,planner_runtime,planner_policy_time,planner_policy_size
    # else:
    #     return problem_filename,automata_runtime,automaton_sizes,subformulae_sizes,planner_runtime,-1,-1
    return problem_filename,automata_runtime,automaton_sizes,subformulae_sizes,planner_runtime,n_autstate_variables,planner_policy_size

def main(opts):
    # Iteratively compiles the LTL synthesis problem into FOND problems until
    # a solution is found
    
    import os
    # Transforming the specification into automata
    parser = Parser(opts.tlsf)
    automata_alanet = parser.get_alanet(opts)
    
    # Running iterated searches for a strong cyclic solution
    range_inf = 0
    range_sup = opts.k_coBuchi + 1
    if opts.game_type == "nbw":
        range_inf = 1
        
    for k in range(range_inf, range_sup):
        print("Searching solution with k = %s" % k)
        opts.k_coBuchi = k
        
        assert(opts.planner in ["mynd", "prp"])
        if opts.planner == "mynd":
            raise("SAS+ not supported")
            n_fluents,n_actions = syn2sasp.main(opts, automata_alanet)
        else:
            assert(opts.planner == "prp")
            n_fluents,n_actions = syn2pddl.main(opts, automata_alanet)
            
        #continue
        # Run Planner
        print("Running Planner")
        planner = Planner(opts)
        solution_found = planner.run(opts.planner,opts.output_domain, opts.output_instance)
        
        
        determined = False
        assert( opts.game_type == "nbw")
        if solution_found:
            determined = True
            if opts.game_semantics != opts.semantics:
                print("Not Realizable. Certificate found for k=%s" %k)
                isReal = False
            else:
                assert(opts.game_semantics == opts.semantics)
                print("Realizable. Solution found for k=%s" %k)
                isReal = True
        else:
            assert(not solution_found)
            print("Strong cyclic solution not found")
        
        
        if determined:
            break
                
        
        if SAVE_STATITSTICS:
            stats = statistics(automata_alanet,planner,solution_found)
            f = open('results.tsv','a')
            # print("%s\t%s" % (opts.config,'\t'.join(str(x) for x in stats)))
            f.write("%s\t%s\t%s\t%s\t%s\t%s\n" % (opts.config,'\t'.join(str(x) for x in stats), n_fluents, n_actions, opts.planner,isReal))
            f.close()
        # break
    
    if not solution_found:
        print("Solution not found with k <= %s" % k)
        
if __name__ == "__main__":
    import options
    
    opts = options.parse_options()
    main(opts)


