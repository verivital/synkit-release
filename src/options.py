# This file is part of SynKit, a tool for LTL synthesis via automated planning.
# Copyright (C) 2018  Alberto Camacho <acamacho@cs.toronto.edu>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import configparser
from argparse import ArgumentParser

CONFIG_FILENAME = 'defaults.cfg'

def str2bool(string):
    return string.lower() not in ("no", "false", "f", "0", "none")

def parse_options():
    config = configparser.ConfigParser(allow_no_value=True)
    config.read(os.path.join(os.path.dirname(os.path.realpath(__file__)), CONFIG_FILENAME))

    parser = ArgumentParser()

    parser.register('type','bool',str2bool) # add type keyword to registries

    
    
    parser.add_argument('--config',
        action="store", dest="config",
        choices=['MealyLTLSynthesis', 'MealyLTLRealizability', 'MealyLTLCertificate', 'MealyLTLUnrealizability',
        'MooreLTLSynthesis', 'MooreLTLRealizability', 'MooreLTLCertificate', 'MooreLTLUnrealizability',
        'MealyLTLfSynthesis', 'MealyLTLfCertificate', 'MooreLTLfSynthesis', 'MooreLTLfCertificate'],
        help="Selects a predefined configuration. Individual options can be overriden.",
        default='DEFAULT')
    
    # # read command line options to set the predefined configuration
    import sys
    input_args = sys.argv[1:]
    (opts, args) = parser.parse_known_args(input_args)
    default_config = opts.config

    # add the rest of the options
    parser.add_argument('--tlsf',
        required=True, action="store", dest="tlsf",
        help="Path to the TLSF file containing the specification")
        
    parser.add_argument('--output-prefix',
        required=True, action="store", dest="output_prefix",
        help="Output prefix name of the PDDL domain and instance, and output files")
    
    parser.add_argument('--split-spec',
        action="store", dest="split_spec",
        choices=['none', 'tlsf', 'nnf', 'tlsf_components'],
        help="Split the specification into a set of formulae (none,tlsf,nnf,gradual,tlsf_components)",
        default=config.get(default_config, "split_spec"))
    
    parser.add_argument('--aut-type',
        action="store", dest="aut_type",
        choices=['ucw', 'nbw', 'nfw'],
        help="Type of automaton",
        default=config.get(default_config, "aut_type"))
    
    parser.add_argument('--game-type',
        action="store", dest="game_type",
        choices=['ukcw', 'nkbw', 'u0cw', 'nfw','nbw'],
        help="Type of game",
        default=config.get(default_config, "game_type"))
        
    parser.add_argument('--k-coBuchi',
        action="store", dest="k_coBuchi", type=int,
        help="Sets the parameter k of the k-coBuchi automata",
        default=config.getint(default_config, "k_coBuchi", fallback=0))
    
    parser.add_argument('--planner-path',
        action="store", dest="planner_path",
        help="Path to the FOND planner script",
        default=config.get(default_config, "planner_path"))
 
    parser.add_argument('--planner',
        action="store", dest="planner",
        choices=['prp', 'fip', 'mynd', 'mynd_sc'],
        help="FOND planner",
        default=config.get(default_config, "planner"))
    
    parser.add_argument('--output-domain',
        action="store", dest="output_domain",
        help="Output name of the PDDL domain",
        default=config.get(default_config, "output_domain", fallback=0))
    
    parser.add_argument('--output-instance',
        action="store", dest="output_instance",
        help="Output name of the PDDL instance",
        default=config.get(default_config, "output_instance", fallback=0))
    
    parser.add_argument('--fixed-var-ordering',
        dest="fixed_var_ordering", type=str2bool,
        help="Fixes the order of assignments to variables in the PDDL problem",
        default=config.getboolean(default_config, "fixed_var_ordering", fallback=0))
    
    parser.add_argument('--ltl2aut',
        action="store", dest="ltl2aut",
        choices=['ltl3ba', 'spot', 'ltl2sa'],
        help="Tool used to transform LTL into automata",
        default=config.get(default_config, "ltl2aut"))

    parser.add_argument('--from-ltlf',
        dest="from_ltlf", type=str2bool,
        help="Transform LTL-f specification into an equivalent LTL formula",
        default=config.getboolean(default_config, "from_ltlf", fallback=0))

    parser.add_argument('--check-unrealizability',
        dest="check_unrealizability", type=str2bool,
        help="Transform LTL-f specification into an equivalent LTL formula",
        default=config.getboolean(default_config, "check_unrealizability", fallback=0))

    parser.add_argument('--solve-dual',
        dest="solve_dual", type=str2bool,
        help="Solve the dual problem that inverts the sementics (Mealy/Moore) and negates the specification formula.",
        default=config.getboolean(default_config, "solve_dual", fallback=0))

    parser.add_argument('--semantics',
        action="store", dest="semantics",
        choices=['mealy', 'moore'],
        help="Semantics of the specification (Mealy/Moore).",
        default=config.get(default_config, "semantics"))

    parser.add_argument('--game-semantics',
        action="store", dest="game_semantics",
        choices=['mealy', 'moore'],
        help="Semantics of the game (Mealy/Moore).",
        default=config.get(default_config, "game_semantics", fallback=0))

    # read rest of command line options and override the predefined configuration
    (opts, args) = parser.parse_known_args(input_args)

    if not opts.output_domain:
        assert(opts.output_prefix)
        opts.output_domain = "%s_domain.pddl" % opts.output_prefix
    if not opts.output_instance:
        assert(opts.output_prefix)
        opts.output_instance = "%s_instance.pddl" % opts.output_prefix
        
    # check that unrealizability detection is implemented for the type of automaton
    if opts.check_unrealizability:
        assert(opts.aut_type == "nfw")

    # checking that (infinite) LTL synthesis compilations are used
    # when the 'from_ltlf' flag is enabled
    if opts.from_ltlf:
        assert(opts.aut_type == 'nfw')
    return opts
