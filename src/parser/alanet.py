#!/usr/bin/env python3

from translator.automata import get_automaton,get_automata
from os import system
import time

SAFE_AUT_SIZE_THRESHOLD = 30
# SAFE_FML_SIZE_THRESHOLD = 20

class ALANet:
    def __init__(self, specs_dict, opts):
        self.subtree_dict = {}
        self.automata_dict = {}
        self.automata = []
        aut_id = 0
        self.automata_time = None
        start = time.time()

        # auxiliar for statistics
        self.subformulae_dict = {}
        
        legal_keys = ['neg_initially', 'preset', 'neg_environment', 'agent']
        for key in specs_dict:
            assert(key in legal_keys)
            
            newkey = key
            # negate key
            if opts.check_unrealizability:
                # new_legal_keys = ['initially', 'neg_preset', 'environment', 'neg_agent']
                if key[0:4] == 'neg_':
                    newkey = key[4:]
                else:
                    newkey = 'neg_%s' % key

            self.automata_dict[newkey] = []

            self.subtree_dict[newkey] = []
            self.subformulae_dict[newkey] = []
            
            nspecs = 0
            for spec in specs_dict[key]:
                nspecs += 1
                
                # if opts.aut_type == "ucw":
                #     # negate spec
                #     if opts.solve_dual:
                #         spec = negate(spec)
                        
                # if opts.aut_type == "nbw":
                #     if opts.solve_dual:
                #         spec = negate(spec)
                    
                # negate spec
                if opts.check_unrealizability:
                    spec = negate(spec)

                # print(spec)
                assert(opts.split_spec in ["nnf","gradual","tlsf","none"])
                if opts.split_spec in ["nnf","tlsf","none"]:
                    if opts.split_spec == "nnf":
                        varformula,varlist,automata = get_automata(spec, opts.aut_type, opts.ltl2aut, opts.from_ltlf)
                    else:
                        assert(opts.split_spec in ["tlsf","none"])
                        varformula,varlist,automata = '_G100', '[_G100]', [get_automaton(spec, opts.aut_type, opts.ltl2aut, opts.from_ltlf)]
                    varlist = varlist.replace('[','').replace(']','').strip().split(',')
                    temp_dict = {}
                    assert(len(automata) == len(varlist))
                    for i in range(len(automata)):
                    # for automaton in automata:
                        self.automata.append(automata[i])
                        self.automata_dict[newkey].append(aut_id)
                        temp_dict[varlist[i]] = aut_id
                        print("\tGenerated %s %s automaton %d of size %d for spec %s" % (newkey,opts.aut_type,aut_id, automata[i].num_states(), nspecs))
                        aut_id += 1 

                    # create a subtree that represents the formula
                    # replace the automaton IDs given by LTL2NFA with aut_id's
                    root_node = Node(varformula, temp_dict)
                    self.subtree_dict[newkey].append(root_node)
    
                    self.subformulae_dict[newkey].append(spec)

                else:
                    assert(opts.split_spec in ["gradual"])
                    # from translator.ltl2sa import ltl2nnf
                    from translator.ltlfkit import ltlf2nnf as ltl2nnf
                    nnf_formula = ltl2nnf.ltl2nnf(spec)
                    assert(len(self.automata) == 0)
                    # print(nnf_formula)
                    root_node = DynamicNode(nnf_formula, opts, self.automata)
                    # root_node = DynamicNode(spec, opts, self.automata)
                    self.subtree_dict[newkey].append(root_node)
                    

        self.automata_time = time.time() - start
        print("Automata generation time: %s" % self.automata_time)
        
        self.automata_dict_reversed = {}
        for key in self.automata_dict:
            for aut_id in self.automata_dict[key]:
                self.automata_dict_reversed[aut_id] = key
    
            
def negate(spec):
    return "!(%s)" % spec.strip()
    
def get_subformulae(fml):
    start = 0
    cur_idx = 0
    subformulae = []
    parenth_count = 0
    while cur_idx < len(fml):
        if fml[cur_idx] == '(':
            parenth_count += 1
        elif fml[cur_idx] == ')':
            parenth_count -= 1
        
        if fml[cur_idx] == ',' and parenth_count == 0:
            assert(cur_idx > 0)
            subformulae.append(fml[start:cur_idx])
            start = cur_idx + 1
        cur_idx += 1
    assert(parenth_count == 0)
    subformulae.append(fml[start:cur_idx])
    return subformulae
    
    # if len(fml) > 3:
    #     if fml[0:3] == "or(":
    #         start = 2
    # if len(fml) > 4:
    #     if fml[0:4] == "and(":
    #         start = 3
    if start > 0:
        cnt = 0
        for i in range(start,len(fml)):
            if fml[i] == "(":
                cnt += 1
            elif fml[i] == ")":
                cnt -= 1
            if cnt == 0:
                assert(fml[i+1] == ',')
                subformulae = [fml[0:i+1], fml[i+2:]]
                break
    else:
        assert(start == 0)
        for i in range(start,len(fml)):
            if fml[i] == ",":
                subformulae = [fml[0:i], fml[i+1:]]
                break
    return subformulae

def split_fml(fml):
    if fml.startswith("or("):
        connective = "or"
        subformulae = get_subformulae(fml[3:-1])
        # subformulae = get_subformulae(fml)
        return connective,subformulae
    elif fml.startswith("and("):
        connective = "and"
        subformulae = get_subformulae(fml[4:-1])
        # subformulae = get_subformulae(fml)
        return connective,subformulae

    connective = ""
    subformulae = [fml]
    return connective,subformulae


class Node:
    def __init__(self, fml, temp_dict):
        self.fml = fml
        self.aut_id = None
        self.siblings = None
        
        # print(fml)
        # print(split_fml(fml))
        connective, subformulae = split_fml(fml)
        self.connective = connective
        
        if connective == "":
            assert(len(subformulae) == 1)
            self.aut_id = temp_dict[subformulae[0]]
            self.node_id = str(temp_dict[subformulae[0]])
            # print(self.aut_id)
        
        else:
            self.siblings = []
            for subformula in subformulae:
                self.siblings.append(Node(subformula, temp_dict))
                
            self.node_id = "%s_%s" % (connective, "_".join(sibling.node_id for sibling in self.siblings))
            # print(self.node_id)q
    

class DynamicNode:
    def __init__(self, fml, opts, automata):
        self.fml = fml
        self.aut_id = None
        self.siblings = None

        successfully_converted = True
        # import traceback
        try:
            # print(fml);exit()
            automaton = get_automaton(fml, opts.aut_type, opts.ltl2aut, opts.from_ltlf)
            if automaton.num_states() > SAFE_AUT_SIZE_THRESHOLD:
                # from translator.ltl2sa import ltl2nnf
                from translator.ltlfkit import ltlf2nnf as ltl2nnf
                connective,left,right = ltl2nnf.break_nnf(fml) 
                # perform a balanced decomposition of the formula wrt connective
                left,right = balanced_break_by_op(fml,connective)
                
                if connective == "": # the formula can not be decomposed, but an automaton was successfully produced
                    successfully_converted = True
                else:
                    successfully_converted = False
        except Exception:
            # print(traceback.format_exc())
            successfully_converted = False

        if successfully_converted:
            self.connective = ""
            self.aut_id = len(automata)
            print("\tGenerated automaton of size %d" % (automaton.num_states()))
            automata.append(automaton) 
            self.node_id = str(self.aut_id)

        else:
            print("Decomposing formula of size %s." % formula_size(fml))
            # from translator.ltl2sa import ltl2nnf
            from translator.ltlfkit import ltlf2nnf as ltl2nnf
            connective,left,right = ltl2nnf.break_nnf(fml)
            # performs a balanced decomposition of the formula, with similar lengths
            # connective,left,right = balanced_break(connective,left,right)
            left,right = balanced_break_by_op(fml,connective)

            print("... into formulas of sizes %s and %s." % (formula_size(left), formula_size(right)))

            if connective == "":
                print("Error: subformula can not be decomposed and cannot be converted into automata")
                exit()
            self.connective = connective
            self.siblings = []
            for subformula in [left,right]:
                self.siblings.append(DynamicNode(subformula, opts, automata))
                
            self.node_id = "%s_%s" % (connective, "_".join(sibling.node_id for sibling in self.siblings))
            print("Node %s ID: %s" % (self.connective, self.node_id))

def balanced_break_by_op(fml,connective):
    subformulae = break_by_op(fml,connective) 
    left,right = split_balanced(subformulae)
    left = convert_op(connective).join(left)
    right = convert_op(connective).join(right)
    return left,right
    
def split_balanced(subformulae):
    if len(subformulae) < 2:
        return subformulae
        
    else:
        subformulae = sorted(subformulae, key=formula_size)
        left = []
        sum_left = 0
        right = []
        sum_right = 0
        # total_len = sum(formula_size(fml) for fml in subformulae)
        k = len(subformulae) -1
        while k >=0:
            if sum_left < sum_right:
                left.append(subformulae[k])
                sum_left += formula_size(subformulae[k])
            else:
                right.append(subformulae[k])
                sum_right += formula_size(subformulae[k])
            k -= 1
        return left,right
            

def formula_size(fml):
    return sum(fml.count(op) for op in ['X','G', 'U', 'F', 'W', 'N', 'R','||', '&&'])

def break_by_op(fml,op):
    if op == "":
        return [fml]
    
    from translator.ltlfkit import ltlf2nnf as ltl2nnf
    subformulas = []
    connective,left,right = ltl2nnf.break_nnf(fml) 
    if connective == op:
        return break_by_op(left,op) + break_by_op(right,op)
    else:
        return [fml]
        

# def balanced_break(connective,left,right):
#     leftsize = formula_size(left)
#     rightsize = formula_size(right)
#     # performs a balanced decomposition of the formula, with similar lengths
#     if connective != "":
#         if leftsize < rightsize:
#             from translator.ltlfkit import ltlf2nnf as ltl2nnf
#             # right = ltl2nnf.ltl2nnf(right)
#             # print(right)
#             rightconnective,rightleft,rightright = ltl2nnf.break_nnf(right)
#             assert(rightconnective != "")
#             # newleft = "((%s)%s(%s))" % (left,convert_op(connective),rightleft)
#             # newright = "((%s)%s(%s))" % (left,convert_op(connective),rightright)

#             newleft = compact_join(connective,left,rightleft)
#             newright = compact_join(connective,left,rightright)
#             if True:
#                 ltl2nnf.break_nnf(left)
#                 ltl2nnf.break_nnf(rightleft)
#                 print(len(left))
#                 print(len(rightleft))
#                 print(len(newleft))
#                 ltl2nnf.break_nnf(newleft)
#                 ltl2nnf.break_nnf(newright)
#             newconnective = rightconnective
#             # print(newconnective)
#             return balanced_break(newconnective,newleft,newright)
#         elif leftsize > rightsize:
#             from translator.ltlfkit import ltlf2nnf as ltl2nnf
            
#             leftconnective,leftleft,leftright = ltl2nnf.break_nnf(left)
#             assert(leftconnective != "")
#             # newleft = "((%s)%s(%s))" % (leftleft,convert_op(connective),right)
#             # newright = "((%s)%s(%s))" % (leftright,convert_op(connective),right)

#             newleft = compact_join(connective,leftleft,right)
#             newright = compact_join(connective,leftright,right)
#             if True:
#                 ltl2nnf.break_nnf(newleft)
#                 ltl2nnf.break_nnf(newright)
#             newconnective = leftconnective
#             # print(newconnective)
#             # print(newleft)
#             # print(newright)
#             return balanced_break(newconnective,newleft,newright)
#     #  if leftsize == rightsize or connective == "":
#     return connective,left,right
    
# def compact_join(connective,left,right):
#     assert(connective != "")
#     assert(connective in ["or","and"])
#     # print("hello")
#     # print(connective)
#     # print("bye")
#     # return "((%s)%s(%s))" % (left,convert_op(connective),right)

#     # the idea is to extract a common factor in the composition of formulas
#     from translator.ltlfkit import ltlf2nnf as ltl2nnf

#     leftconnective,leftleft,leftright = ltl2nnf.break_nnf(left)
#     rightconnective,rightleft,rightright = ltl2nnf.break_nnf(right)
#     if rightconnective == leftconnective:
#         if leftleft == rightleft:
#             return "((%s)%s(%s%s%s))" % (rightleft,convert_op(rightconnective),leftright,convert_op(connective),rightright)
#         elif leftright == rightright:
#             return "((%s%s%s)%s(%s))" % (leftleft,convert_op(connective),rightleft,convert_op(leftconnective),rightright)
#     # else:
#     return "((%s)%s(%s))" % (left,convert_op(connective),right)
    
    
def convert_op(op):
    assert(op in ["or", "and"])
    if op == "or":
        return " || "
    elif op == "and":
        return " && "