# This file is part of SynKit, a tool for LTL synthesis via automated planning.
# Copyright (C) 2018  Alberto Camacho <acamacho@cs.toronto.edu>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time

class Planner:
    def __init__(self,opts):
        self.opts = opts
        self.stats = None
        
    def run(self,planner,domain,instance):
        if planner == "prp":
            import planner.prp as planner
        elif planner == "mynd":
            import planner.mynd as planner
        elif planner == "mynd_sc":
            import planner.mynd_sc as planner
        elif planner == "fip":
            import planner.fip as planner
        else:
            raise("Error: planner not supported")
        
        self.stats = planner.run(self.opts.planner_path,domain,instance)
        return self.stats.solution_found
        
    def runtime(self):
        return self.stats.runtime
        
    def policy_size(self):
        return self.stats.policy_size
        
    def policy_time(self):
        return self.stats.policy_time
        
    def nodes_visited(self):
        return self.stats.nodes_visited
        
    def n_variables(self):
        return self.stats.n_variables
    
    def n_autstate_variables(self):
        return self.stats.n_autstate_variables
        
    def solution_found(self):
        return self.stats.solution_found
        
    def plan():
        return self.plan